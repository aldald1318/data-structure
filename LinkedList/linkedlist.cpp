#include <iostream>
#include <stdio.h>
using namespace std;

#define MAX_LIST_SIZE 100 // 리스트의 최대크기

typedef int element; // 항목의 정의

typedef struct
{
	element array[MAX_LIST_SIZE]; // 배열 정의
	int size; // 현재 리스트에 저장된 항목들의 개수
}ArrayListType;


void Error(const char* msg)
{
	fprintf(stderr, "%s\n", msg);
	exit(1);
}

void Init(ArrayListType& L)
{
	L.size = 0;
}

int Is_empty(ArrayListType& L)
{
	return L.size == 0;
}

int Is_Full(ArrayListType& L)
{
	return L.size == MAX_LIST_SIZE;
}

element Get_Entry(ArrayListType& L, int pos)
{
	if (pos < 0 || pos >= L.size) 
		Error("위치 오류");

	return L.array[pos];
}

void Print_List(ArrayListType& L)
{
	for (int i = 0; i < L.size; ++i)
		printf("%d -> ", L.array[i]);
	printf("\n");
}

void Insert_First(ArrayListType& L, element item)
{
	if (L.size == 0)
	{
		L.array[0] = item;
		L.size++;
		return;
	}

	for (int i = (L.size - 1); i >= 0; --i)
		L.array[i + 1] = L.array[i];

	L.array[0] = item;
	L.size++;
}

void Insert_Last(ArrayListType& L, element item)
{
	if (L.size >= MAX_LIST_SIZE) {
		Error("list overflow");
	}
	L.array[L.size++] = item;
}

void Insert(ArrayListType& L, int pos, element item)
{
	if (!Is_Full(L) && 
		(pos >= 0) && 
		(pos <= L.size))
	{
		for (int i = (L.size - 1); i >= pos; --i)
			L.array[i + 1] = L.array[i];
		L.array[pos] = item;
		L.size++;
	}
}

element Delete(ArrayListType& L, int pos)
{
	element item;

	if (pos < 0 || pos >= L.size)
		Error("위치 오류");

	item = L.array[pos];
	for (int i = pos; i < (L.size - 1); ++i)
		L.array[i] = L.array[i + 1];
	L.size--;
	return item;
}

int main()
{
	ArrayListType list;

	Init(list);

	Insert(list, 0, 10);
	Print_List(list);
	//Insert(list, 0, 20);
	//Print_List(list);
	//Insert_Last(list, 40);
	//Print_List(list);

	//Delete(list, 0);
	//Print_List(list);

	Insert_First(list, 50);
	Print_List(list);

	return 0;
}